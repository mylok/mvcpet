﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FlcalPriceRD;

namespace MvcFocalPrice
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "search",
                "search/{keyword}",
                new { Controller = "Search", action = "Index" }
                );

            routes.MapRoute(
                "category",
                "category/{categoryid}/{alias}",
                new { Controller = "Categories", action = "index", alias = UrlParameter.Optional},
                new { alias = new CategoryMatchConstraint() }
                );

            routes.MapRoute(
                "producttoshoppingcart",
                "shoppingcart/{qty}",
                new { Controller = "Product", action = "index1", qty = UrlParameter.Optional }
                );

            routes.MapRoute(
                "product",
                "product/{productid}/{productname}",
                new { Controller = "Product", action = "index", productid = UrlParameter.Optional, productname = UrlParameter.Optional}
                );

            routes.MapRoute(
                "default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }

    public class CategoryMatchConstraint : IRouteConstraint 
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var category = values[parameterName].ToString();
            if (string.IsNullOrEmpty(category))
            {
                return false;
            }
            category = category.Replace("-", string.Empty).Replace("&", string.Empty).Replace(" ", string.Empty).Trim();
            using (var db = new FocalpriceEntities())
            {
                var ismatch =  db.Categories.Any(c => c.Name.Replace("-", string.Empty).Replace("&", string.Empty).Replace(" ", string.Empty).Trim() == category);
                return ismatch;
            }
        }
    }
}