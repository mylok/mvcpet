using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mvc.Mailer;
using System.Net.Mail;

namespace MvcFocalPrice.Mailers
{ 
    public interface IUserMailer
    {
				
		MailMessage Welcome();
		
		MailMessage PasswordReset();

        MailMessage OrderInfo(int orderid, string usermail);

        MailMessage PasswordRetrieve(string usermail, string password);

        MailMessage Errorlog(string errormessage);

        MailMessage TrackingInfo(int orderid, string trackingnumber);
	}
}