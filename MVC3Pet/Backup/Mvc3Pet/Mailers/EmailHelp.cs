﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;


/// <summary>
/// Summary description for EmailHelp
/// </summary>
public class EmailHelp
{
    public SmtpClient mailClient { get; set; }

    public string mailfrom { get; set; }

    public List<string> cclist { get; set; }

    public string attachmentpath { get; set; }

    public EmailHelp()
    {
        mailClient = new SmtpClient();
        // mailClient.Host = "smtpout.secureserver.net";
        mailClient.Host = "relay-hosting.secureserver.net";

        // mailClient.Port = 80;
        mailClient.Port = 25;
        mailfrom = "info@petsmartpm.com";
    }

    public void SendGmail(string from, string to, string title, string content, string attachmentpath)
    {
        var message = new MailMessage(from, to, title, content) { IsBodyHtml = true };
        if (attachmentpath != null)
        {
            var attachment = new Attachment(attachmentpath);
            message.Attachments.Add(attachment);
        }

        if (cclist != null)
        {
            foreach (var item in cclist)
            {
                message.CC.Add(item);
            }
        }

        var mailClient = new SmtpClient();

        mailClient.Credentials = new System.Net.NetworkCredential("petsmartpm@gmail.com", "Bloodman@123");
        mailClient.EnableSsl = true;
        mailClient.Port = 587;
        mailClient.Host = "smtp.gmail.com";

        mailClient.Send(message);
        message.Dispose();
    }

    /// <summary>
    /// old method
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <param name="title"></param>
    /// <param name="content"></param>
    /// <param name="attachmentpath"></param>
    /// <param name="cc"></param>
    public void Send(string from, string to, string title, string content, string attachmentpath, List<string> cc)
    {
        // string mailServerName = System.Web.Configuration.WebConfigurationManager.AppSettings["godaddyemailserver"].ToString();
        var password = "123456";
        if (from == "luke@printingsuppliesusa.com")
        {
            password = "castle11";
        }

        var message = new MailMessage(from, to, title, content) { IsBodyHtml = true };
        if (attachmentpath != null)
        {
            var attachment = new Attachment(attachmentpath);
            message.Attachments.Add(attachment);
        }
        // var mailClient = new SmtpClient { Host = mailServerName };
        var mailClient = new SmtpClient();
        mailClient.Credentials = new System.Net.NetworkCredential(from, password);
        mailClient.Host = "smtpout.secureserver.net";
        mailClient.Port = 80;
        if (cc != null)
        {
            foreach (var item in cc)
            {
                message.CC.Add(item);
            }
        }

        mailClient.Send(message);
        message.Dispose();
    }

    public void Send(string to, string title, string content)
    {
        //var password = "bloodman@123";

        //mailClient.Credentials = new System.Net.NetworkCredential(mailfrom, password);
        //mailClient.EnableSsl = false;

        var message = new MailMessage(mailfrom, to, title, content) { IsBodyHtml = true };
        message.Priority = MailPriority.High;
        if (attachmentpath != null)
        {
            var attachment = new Attachment(attachmentpath);
            message.Attachments.Add(attachment);
        }

        if (cclist != null)
        {
            foreach (var item in cclist)
            {
                message.CC.Add(item);
            }
        }

        mailClient.Send(message);
        message.Dispose();
    } 
}
