﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;
using FlcalPriceRD.ViewModels;

namespace MvcFocalPrice.Controllers
{
    public class CategoriesController : Controller
    {
        //
        // GET: /Categories/

        public ActionResult Index(string exp, int pageno = 1, int categoryid = 2, string orderby = "lowestprice")
        {
            var db = new FocalpriceEntities();
            ViewBag.sort = orderby;
            ViewBag.exp = exp;
            var help = new Help();
            ViewBag.pageno = pageno;
            var parentid = db.Categories.Single(c => c.ID == categoryid).ParentID;
            ViewBag.catedescript = (parentid != 1 && parentid != null) ? db.Categories.Single(c => c.ID == parentid) : db.Categories.Single(c => c.ID == categoryid);
            var categoryproductcountlist = db.Categories.Where(c => c.ID == categoryid && c.IsActive == true).Select(c => new categoryproductcount { CategoryName = c.Name, ID = c.ID, ProductCount = db.Products.Where(xc => xc.CategoryID == categoryid && (xc.IsAvailable == null || xc.IsAvailable == true)).Count(), ParentID = c.ParentID }).ToList();
            // var categoryproductcountlist1 = db.Categories.Where(c => c.ID == categoryid && c.IsActive == true).Select(c => new categoryproductcount { CategoryName = c.Name, ID = c.ID, ProductCount = c.productcategorymatches.Where(pd => pd.Product.IsAvailable == true || pd.Product.IsAvailable == null).Count(), ParentID = c.ParentID }).ToList();

            if (db.Categories.Any(c => c.ParentID == categoryid))
            {
                categoryproductcountlist = db.Categories.Where(c => c.ParentID == categoryid && c.IsActive == true).Select(c => new categoryproductcount { CategoryName = c.Name, ID = c.ID, ProductCount = c.productcategorymatches.Where(pd=>pd.Product.IsAvailable == true || pd.Product.IsAvailable == null).Count(), ParentID = c.ParentID }).ToList();
            }
            ViewBag.categoryproductcountlist = categoryproductcountlist;
            var tcoll = new List<int>();
            help.GetCategoryList(categoryid, tcoll);
            ViewBag.totalpage = Math.Ceiling((double)db.Products.Count(p => (p.IsAvailable == true || p.IsAvailable == null) && tcoll.Contains(p.CategoryID)) / 30);

            var categorycoll = db.Categories.Where(c => c.ParentID == 1 && c.IsActive == true).ToList();
            var productcoll = new List<ProductListViewModel>();
            var productidcoll = db.productcategorymatches.Where(m => tcoll.Contains(m.categoryid)).Select(p => p.productid);
            if (orderby == "lowestprice")
            {
                productcoll = (from pc in db.Products.Where(p => productidcoll.Contains(p.ID) && (p.IsAvailable == true || p.IsAvailable == null)).OrderBy(p => p.Price).Skip((pageno - 1) * 30).Take(30)
                                   let picimg = pc.ProductImages.FirstOrDefault(x => x.IsFeature == true)
                                   select new ProductListViewModel { ID = pc.ID, Name = pc.Name, Price = pc.Price, picname = picimg == null ? string.Empty : picimg.Name }).ToList();
            }
            else
            {
                productcoll = (from pc in db.Products.Where(p => productidcoll.Contains(p.ID) && (p.IsAvailable == true || p.IsAvailable == null)).OrderByDescending(p => p.Price).Skip((pageno - 1) * 30).Take(30)
                                   let picimg = pc.ProductImages.FirstOrDefault(x => x.IsFeature == true)
                                   select new ProductListViewModel { ID = pc.ID, Name = pc.Name, Price = pc.Price, picname = picimg == null ? string.Empty : picimg.Name }).ToList();
            }

            ViewBag.producttotal = db.productcategorymatches.Where(p => tcoll.Contains(p.categoryid) && (p.Product.IsAvailable == true || p.Product.IsAvailable == null)).Count();

             var homeview = new HomeViewModel
            {
                Productcoll = productcoll,
                Categorycoll = categorycoll
            };
            return View(homeview); 
        }
    }

    public class categoryproductcount
    {
        public string CategoryName { get; set; }
        public int ProductCount { get; set; }
        public int ID { get; set; }
        public int? ParentID { get; set; }
    }

}
