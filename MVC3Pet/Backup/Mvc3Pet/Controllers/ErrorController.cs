﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;

namespace MvcFocalPrice.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {
            var db = new FocalpriceEntities();
            var categorycoll = db.Categories.Where(c => c.ParentID == 1 && c.IsActive == true).ToList();
            return View(categorycoll);
        }

    }
}
