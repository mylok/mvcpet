﻿$(function () {
    //通知
    var notices = '<em class="fb">Dear valued customer</em>,Due to China’s traditional lunar New Year (Jan.22nd to Jan. 24th 2012),\
				  any orders placed during this period with Focalprice.com will be delayed 3-5 days averagely. For further details, please\
				 <a class="cf50ulin" href="http://forums.focalprice.com/showtopic-36974.aspx">click here</a>.\
				  We apologize for any inconvenience caused at this time.',
		notices = '<p><span>' + notices + '</span></p><a class="close" href="javascript:void(0);"></a>';
 
   
    $.getJSON('http://promotion.focalprice.com/promotionservices/QueryDateTimeJSonP?jsoncallback=?', function (data) {
        var nowTime = data.Date;
        nowTime = nowTime.replace("-", "/").replace("-", "/"),
			    nowTime = new Date(nowTime);
        var year = nowTime.getFullYear(),
				month = nowTime.getMonth() + 1,
				day = nowTime.getDate(),
				endTime = year + "/" + month + "/" + day + " " + "23:59:59",
			
				endTime = new Date(endTime);
		if(month == 1 && day <= 24){
			   $("#topmenu").before('<div class="topNotice" id="transforDomainClose" style="z-index:300">' + notices + '</div>');
				$(".close").live("click", function(){
				$("#transforDomainClose").hide();	
				});
		}
        var leftsecond = parseInt(endTime.getTime() - nowTime.getTime());
        new leftTime('', '', '', '', document.getElementById('leftTime'), leftsecond, function () { });
    });
    //$("form").submit(function () { $(this).find("input[type=submit]").attr("disabled", "disabled"); $(this).find("input[type=submit]").addClass("disable"); });
    //模拟select
    function FillSearchTopCategories(callback) {
        if ($("#searchForm .selectPanelList").children().length > 1) { return; }
        $.getJSON("/AllTopCategories", function (jsonData) {
            var thisObj = $("#searchForm .selectPanelList");
            var first = thisObj.children().first();
            $.each(jsonData, function (n,v) {
                var clone = first.clone();
                clone.attr("id",  n);
                clone.html(v);
                thisObj.append(clone);
            });
            callback();
        });
    }
    function SearchTopCategoriesHover() {
        $(".selectPanel").children().find('li').hover(function () { $(this).addClass('selecthover') }, function () { $(this).removeClass('selecthover') });
    }
    function SearchTopCategoriesChanged() {
        $('ul.selectPanelList li').click(function () {
            $(this).parents('li').find('span').html($(this).html());
            $(this).parents('.selectPanel').find('ul').addClass("hide");
            var selectId = $(this).attr("id").toString();
            if (selectId == null) {
                $("#categoryid").val("");
            }
            else {
                $("#categoryid").val(selectId);
            }
        });
    }
    $('.selectPanelList').hide();
    $(".selectPanel").click(function () {
        FillSearchTopCategories(function () {
            SearchTopCategoriesHover();
            SearchTopCategoriesChanged();
        });
        $(".selectPanelList").toggle();
    });

    $(document).bind('click',
		function (e) {
		    var clickme = $(e.target);
		    if (!clickme.parents().hasClass("selectPanelWarp"))
		        $(".selectPanelList").hide();
		});
    //导航栏下拉
    $(".siginin").hover(function () {
        $(this).addClass("useractive"); $(this).children(".siginin_menu").show();

    }, function () {
        $(this).removeClass("useractive"); $(this).children(".siginin_menu").hide();
    });
    $(".account, .language, .currency").hover(function () {
        $(this).children(".selMenu").addClass("active");
        $(this).children(".down_men").show();
    }, function () {
        $(this).children(".selMenu").removeClass("active");
        $(this).children(".down_men").hide();
    });
    $(".litem").hover(function () {
        $(this).addClass("hoverClass");
        $(this).children(".subitem").show();
    }, function () {
        $(this).removeClass("hoverClass");
        $(this).children(".subitem").hide();
    });
    //deals
    var timeTip;
    $("#dealMenuPanle, #dealMenu").hover(
		function () {
		    clearTimeout(timeTip);
		    $("#dealMenuPanle").show();
		},
		function () {
		    timeTip = setTimeout(function () {
		        $("#dealMenuPanle").hide();
		    }, 200)
		});
    //Recently
    var scrtime;
    $("#revieweditems_box").hover(function () {
        clearInterval(scrtime);
    }, function () {
        scrtime = setInterval(function () {
            var $ul = $("#revieweditems_box .revieweditems_m"),
				liHeight = $ul.find("li:last").height();
            $ul.animate({ marginTop: liHeight + 1 + "px" }, 500, function () {
                $ul.find("li:last").prependTo($ul)
                $ul.find("li:first").hide();
                $ul.css({ marginTop: 0 });
                $ul.find("li:first").fadeIn(1000);
            })
        }, 3000);
    }).trigger("mouseleave");

    //Daily Deals
    $("#dailydeals_items").css({ width: "9980px", position: "absolute", left: "0px", top: "0px" });
    var dailyPro_Num = $("#dailydeals_items .itembox").size(),
  	  SingleW = $("#dailydeals_items .itembox").width() + 1,
	  localNum = 1;
    addLinks(0);
    $(".dailydeals_next").click(function () {
        if (!$("#dailydeals_items").is(":animated")) {
            if (localNum == dailyPro_Num) {
				$(this).addClass("disableClass_r");
                return false;
            } else {
                $("#dailydeals_items").animate({ left: '-=' + SingleW }, "fast");
                $(".dailydeals_prev").removeClass("disableClass");
                addLinks(localNum);
                localNum++;
                if (localNum == dailyPro_Num) {
                    $(this).addClass("disableClass_r");
                }
            }
        }

    });
    $(".dailydeals_prev").click(function () {
        if (!$("#dailydeals_items").is(":animated")) {

            if (localNum == 1) {
                $(this).addClass("disableClass");
                return false;
            } else {
                $("#dailydeals_items").animate({ left: '+=' + SingleW }, "fast");

                localNum--;
                addLinks(localNum - 1);
                $(".dailydeals_next").removeClass("disableClass_r");
                if (localNum == 1) {
                    $(this).addClass("disableClass");
                }
            }
        }
    })
    function addLinks(num) {
        var thisPro = $("#dailydeals_items .itembox ").eq(num),
				saleOff = thisPro.find("#SaleOff").html(),
				Buylinks = thisPro.find(".proImg").find("a").attr("href");
        Buylinks = "/cart/add?productId=" + Buylinks.match(/\/([^\/]*)\//i)[1];
        $("#dealsoff").html("<em>" + saleOff + "</em><br/>OFF");
        $("#buyLinks").attr("href", Buylinks);
    }

    //continue shopping
    /*$(".continueshopping_m").hover(function ()
    {
    if ($(this).find(".itembox").length > 4)
    {
    $(this).children(".continue_pages").children(".continue_prev").fadeIn();
    $(this).children(".continue_pages").children(".continue_next").fadeIn();
    }
    }, function ()
    {
    $(this).children(".continue_pages").children(".continue_prev").fadeOut();
    $(this).children(".continue_pages").children(".continue_next").fadeOut();
    });
    $.ScroolPro({
    wrap: ".continueshopping_m",
    preBtn: ".continue_prev",
    nextBtn: ".continue_next",
    content: ".continue_warp",
    listwrap: ".continue",
    listcon: ".itembox",
    page: 1,
    i: 4
    });*/

    //Exclusively and Albums
    $(".lbox_main_item:odd, .Albums_item:odd").addClass("odd");
    $(".lbox_main_item:last-child").css("border-bottom", "none");
    $('<div class="exclus_tips"><div class="exclus_text"></div></div>').appendTo('body');
    $('.exclusTips').mouseover(function () {
        $('.exclus_tips').css('visibility', 'visible');
        var el = this;
        if (el.title) { el.text = el.title; el.title = ''; }
        myText = el.text;
        $('.exclus_text').html(myText);
    }).mousemove(function (e) {
        $('.exclus_tips').css('top', e.pageY + 15);
        $('.exclus_tips').css('left', e.pageX + 15);
    }).mouseout(function () {
        $('.exclus_tips').css('visibility', 'hidden');
    });

    var AlbumH = -($(".Albums_item").height() + 17),
		ScrollMain = $(".AlbumsMain .Albums_scroll");
    $(".Albums_next").click(function () {
        ScrollMain.animate({ marginTop: AlbumH + "px" }, 500, function () {
            ScrollMain.find(".Albums_item:first").appendTo(ScrollMain);
            ScrollMain.find(".Albums_item:last").hide();
            ScrollMain.css({ marginTop: 0 });
            ScrollMain.find(".Albums_item:last").fadeIn(1000);
        });
    })

})

//清除文本框内容
function clearDefaultText(el, message) {
    var obj = el;
    if (typeof (e1) == "string")
        obj = document.getElementById(id);
    if (obj.value == message) {
        obj.value = "";
        obj.style.color = "#ccc";
    }
    obj.onblur = function () {
        if (obj.value == "") {
            obj.value = message;
            obj.style.color = "#ccc";
        }
    }
    obj.style.color = "#000";
}