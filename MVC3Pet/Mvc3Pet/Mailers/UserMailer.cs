using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mvc.Mailer;
using System.Net.Mail;
using FlcalPriceRD;
using System.Web.Mvc;

namespace MvcFocalPrice.Mailers
{ 
    public class UserMailer : MailerBase, IUserMailer     
	{
		public UserMailer():
			base()
		{
			MasterName="_Layout";
		}

		
		public virtual MailMessage Welcome()
		{
			var mailMessage = new MailMessage{Subject = "Welcome"};
            mailMessage.From = new MailAddress("info@petsmartpm.com");
			
			mailMessage.To.Add("shenxueyu@yahoo.com");
			//ViewBag.Data = someObject;
			PopulateBody(mailMessage, viewName: "Welcome");

            return mailMessage;
		}

		
		public virtual MailMessage PasswordReset()
		{
			var mailMessage = new MailMessage{Subject = "PasswordReset"};
            mailMessage.From = new MailAddress("info@petsmartpm.com");
            mailMessage.Send();	
			//mailMessage.To.Add("some-email@example.com");
			//ViewBag.Data = someObject;
			PopulateBody(mailMessage, viewName: "PasswordReset");

			return mailMessage;
		}

        public virtual MailMessage TrackingInfo(int orderid, string trackingnumber)
        {
            var db = new FocalpriceEntities();
            var order = db.Orders.Single(p => p.OrderID == orderid);
            var carrier = order.Carrier;
            string trackingaddress = string.Empty;
            if (carrier == "USPS")
            {
                trackingaddress = "https://tools.usps.com/go/TrackConfirmAction_input";
            }
            if (carrier == "Royal Mail")
            {
                trackingaddress = "http://www.royalmail.com/track-trace";
            }
            if (carrier == "Posten")
            {
                trackingaddress = "http://www.posten.se/en/Pages/Track-and-trace.aspx?search=";
            }
            if (carrier == "Swisspost")
            {
                trackingaddress = "http://www.swisspost.com/";
            }

            ViewBag.trackingaddress = trackingaddress;

			var mailMessage = new MailMessage{Subject = string.Format("Petsmartpm Order {0} Tracking Information", ("20084" + orderid).ToString())};
            mailMessage.To.Add(order.Email);
            mailMessage.CC.Add("info@petsmartpm.com");
            // mailMessage.To.Add("shenxueyu@yahoo.com");
            mailMessage.From = new MailAddress("info@petsmartpm.com"); 
            var orderdetailcoll = db.OrderDetails.Where(p => p.OrderID == orderid).ToList();
            ViewData.Model = orderdetailcoll;
            ViewBag.order = order;
			PopulateBody(mailMessage, viewName: "ShippingTracking");

			return mailMessage;
        }

        public virtual MailMessage OrderInfo(int orderid, string useremail)
        {
            var db = new FocalpriceEntities();
			var mailMessage = new MailMessage{Subject = string.Format("Petsmartpm Order {0} Information", (20084 + orderid)).ToString()};
            mailMessage.To.Add(useremail);
            mailMessage.From = new MailAddress("info@petsmartpm.com");
            var orderdetailcoll = db.OrderDetails.Where(p => p.OrderID == orderid).ToList();
            // ViewData = new ViewDataDictionary(orderdetailcoll);
            ViewData.Model = orderdetailcoll;
            ViewBag.order = db.Orders.Single(p => p.OrderID == orderid);
			PopulateBody(mailMessage, viewName: "OrderInfo");

			return mailMessage;
        }

        public virtual MailMessage PasswordRetrieve(string useremail, string password)
        {
            ViewBag.password = password;
			var mailMessage = new MailMessage{Subject = "Your Petsmartpm Password Information"};
            mailMessage.From = new MailAddress("info@petsmartpm.com");
            mailMessage.To.Add(useremail);
            PopulateBody(mailMessage, viewName: "PasswordRetrieve");
            return mailMessage;
        }

        public virtual MailMessage Errorlog(string errormessage)
        {
            ViewBag.errormessage = errormessage;
            var mailMessage = new MailMessage {Subject = "Pet Error" };
            mailMessage.From = new MailAddress("info@petsmartpm.com");
            mailMessage.To.Add("shenxueyu@yahoo.com");
            PopulateBody(mailMessage, viewName: "ErrorMessage");

            return mailMessage;
        }
	}
}