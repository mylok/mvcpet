﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;
using MvcFocalPrice.Mailers;
using Mvc.Mailer;
using System.Net.Mail;


namespace MvcFocalPrice.Controllers
{ 
    public class testController : Controller
    {
        private IUserMailer _userMailer = new UserMailer();
        public IUserMailer UserMailer
        {
            get { return _userMailer; }
            set { _userMailer = value; }
        }

        private FocalpriceEntities db = new FocalpriceEntities();

        public ViewResult emailwrap()
        {
            var mailclient = new SmtpClient
            {
                Host = "relay-hosting.secureserver.net",
                Port = 25
            };

            var smtpwrap = new SmtpClientWrapper(mailclient);
            try
            {
                UserMailer.OrderInfo(101, "info@petsmartpm.com").Send(smtpwrap);
                UserMailer.OrderInfo(101, "shenxueyu@yahoo.com").Send(smtpwrap);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString() + "Message : " + ex.Message.ToString();
            }
            return View();
        }


        public ViewResult TestEmail()
        {
            try
            {
                var mailclient = new SmtpClient
                {
                    Host = "relay-hosting.secureserver.net",
                    Port = 25
                };

                var mailclient1 = new SmtpClient
                {
                    Host = "relay-hosting.secureserver.net",
                    Port = 25
                };
                var mailclient2 = new SmtpClient
                {
                    Host = "relay-hosting.secureserver.net",
                    Port = 25
                };
                var smtpwrap = new SmtpClientWrapper(mailclient);
                var smtpwrap1 = new SmtpClientWrapper(mailclient1);
                var smtpwrap2 = new SmtpClientWrapper(mailclient2);
                var emailhelp = new EmailHelp
                {
                    cclist = null,
                    attachmentpath = null
                };

                emailhelp.Send("shenxueyu@yahoo.com", "dd", "This is befoer usermailer");
                UserMailer.OrderInfo(101, "shenxueyu@yahoo.com").Send(smtpwrap);
                UserMailer.OrderInfo(100, "shenxueyu@yahoo.com").Send(smtpwrap1);
                UserMailer.OrderInfo(99, "shenxueyu@yahoo.com").Send(smtpwrap2);
            }
            catch (Exception ex)
            {
                ViewBag.error =  ex.ToString() + "Message : " + ex.Message.ToString();

                var emailhelp1 = new EmailHelp
                {
                    cclist = null,
                    attachmentpath = null
                };
                emailhelp1.Send("shenxueyu@yahoo.com", "mailerror", ex.ToString());

                return View();
            }

            ViewBag.error = "no error";
            //var mailclient = new SmtpClient
            //{
            //    Host = "smtp.gmail.com",
            //    Port = 25,
            //    Credentials = new System.Net.NetworkCredential("suke323@gmail.com", "Bloodman@123"),
            //    EnableSsl = true
            //};

            //var message = new MailMessage("suke323@gmail.com", "shenxueyu@yahoo.com", "testemail", "testemail") { IsBodyHtml = true };
            //mailclient.Send(message);
            return View();
        }


        //
        // GET: /test/

        public ViewResult Index()
        {
            UserMailer.OrderInfo(10, "shenxueyu@yahoo.com").Send();
            var productimages = db.ProductImages.Include("Product");
            return View(productimages.ToList());
        }

        //
        // GET: /test/Details/5

        public ViewResult Details(int id)
        {
            ProductImage productimage = db.ProductImages.Single(p => p.ID == id);
            return View(productimage);
        }

        //
        // GET: /test/Create

        public ActionResult Create()
        {
            ViewBag.ProductID = new SelectList(db.Products, "ID", "Name");
            return View();
        } 

        //
        // POST: /test/Create

        [HttpPost]
        public ActionResult Create(ProductImage productimage)
        {
            if (ModelState.IsValid)
            {
                db.ProductImages.AddObject(productimage);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.ProductID = new SelectList(db.Products, "ID", "Name", productimage.ProductID);
            return View(productimage);
        }
        
        //
        // GET: /test/Edit/5
 
        public ActionResult Edit(int id)
        {
            ProductImage productimage = db.ProductImages.Single(p => p.ID == id);
            ViewBag.ProductID = new SelectList(db.Products, "ID", "Name", productimage.ProductID);
            return View(productimage);
        }

        //
        // POST: /test/Edit/5

        [HttpPost]
        public ActionResult Edit(ProductImage productimage)
        {
            if (ModelState.IsValid)
            {
                db.ProductImages.Attach(productimage);
                db.ObjectStateManager.ChangeObjectState(productimage, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductID = new SelectList(db.Products, "ID", "Name", productimage.ProductID);
            return View(productimage);
        }

        //
        // GET: /test/Delete/5
 
        public ActionResult Delete(int id)
        {
            ProductImage productimage = db.ProductImages.Single(p => p.ID == id);
            return View(productimage);
        }

        //
        // POST: /test/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            ProductImage productimage = db.ProductImages.Single(p => p.ID == id);
            db.ProductImages.DeleteObject(productimage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}