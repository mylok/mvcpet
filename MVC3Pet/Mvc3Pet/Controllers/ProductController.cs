﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;

namespace MvcFocalPrice.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        [HttpGet]
        public ViewResult Index(int productid)
        {
            var db = new FocalpriceEntities();
            var product = db.Products.Single(p => p.ID == productid);
            var productcategoryid = product.CategoryID;
            var categoryidcoll = new List<int?>();
            var help = new Help();
            help.GetParentID((int?)productcategoryid, categoryidcoll);
            var coll = from c in db.Categories.Where(c => categoryidcoll.Contains(c.ID)).OrderBy(c => c.ID)
                       select new CategoryList { CategoryID = c.ID, CategoryName = c.Name.Replace(" & ", "-").Replace(" ", "-") };
            ViewBag.categorycoll = coll.ToList();
            ViewBag.imgcoll = product.ProductImages.ToList();
            return View(product);
        }
    }

    public class CategoryList
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
