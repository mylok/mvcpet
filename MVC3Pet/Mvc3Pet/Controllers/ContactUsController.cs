﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcFocalPrice.Mailers;
using System.Web.Mail;

namespace MvcFocalPrice.Controllers
{
    public class ContactUsController : Controller
    {
        private IUserMailer _userMailer = new UserMailer();
        public IUserMailer UserMailer
        {
            get { return _userMailer; }
            set { _userMailer = value; }
        }
        //
        // GET: /ContactUs/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Index(string txtcomment, string txtfirstname, string txtlastname, string txtemail)
        {
            var issend = true;

            try
            {
            var emailhelp = new EmailHelp
            {
                cclist = null,
                attachmentpath = null
            };
                emailhelp.Send("shenxueyu@yahoo.com", "From PetSmartPM Contact Form", "Email:" + txtemail + "FirstName:" + txtfirstname + " LastName:" + txtlastname + " Content:" + txtcomment);
                emailhelp.Send("info@petsmartpm.com", "From PetSmartPM Contact Form", "Email:" + txtemail + "FirstName:" + txtfirstname + " LastName:" + txtlastname + " Content:" + txtcomment);
            }
            catch (Exception ex)
            {
                issend = false;
            }
            ViewBag.issend = issend;
            return View("Thanks");
        }
    }
}
