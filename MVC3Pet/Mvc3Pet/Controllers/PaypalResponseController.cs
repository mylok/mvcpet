﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Net;
using System.IO;
using FlcalPriceRD;
using Mvc.Mailer;
using MvcFocalPrice.Mailers;
using System.Web.Security;
using System.Net.Mail;

namespace MvcFocalPrice.Controllers
{
    public class PaypalResponseController : Controller
    {
        private IUserMailer _userMailer = new UserMailer();
        public IUserMailer UserMailer
        {
            get { return _userMailer; }
            set { _userMailer = value; }
        }

        string authToken, txToken, query;
        string strResponse;
        //
        // GET: /PaypalResponse/

        public ActionResult Index()
        {
            // Used parts from https://www.paypaltech.com/PDTGen/
            // Visit above URL to auto-generate PDT script
                
            authToken = WebConfigurationManager.AppSettings["PDTToken"];

            //read in txn token from querystring
            txToken = Request.QueryString.Get("tx");
            TempData["Message"] = txToken;
            query = string.Format("cmd=_notify-synch&tx={0}&at={1}", txToken, authToken);

            // Create the request back
            string url = WebConfigurationManager.AppSettings["PayPalSubmitUrl"];
            var req = (HttpWebRequest)WebRequest.Create(url);

            // Set values for the request back
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = query.Length;

            // Write the request back IPN strings
            var stOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
            stOut.Write(query);
            stOut.Close();


            // Do the request to PayPal and get the response
            var stIn = new StreamReader(req.GetResponse().GetResponseStream());
            strResponse = stIn.ReadToEnd();
            stIn.Close();

            // UserMailer.Errorlog(strResponse).Send();

            // sanity check
            // If response was SUCCESS, Update order verified status 
            if (strResponse.StartsWith("SUCCESS"))
            {
                var pdt = PDTHolder.Parse(strResponse);
                var orderid = pdt.InvoiceNumber;
                var db = new FocalpriceEntities();

                if (!string.IsNullOrEmpty(orderid.ToString()))
                {
                    var verifiedorder = db.Orders.Single(o => o.OrderID == orderid);
                    verifiedorder.IsVerify = true;
                    verifiedorder.FirstName = pdt.PayerFirstName;
                    verifiedorder.LastName = pdt.PayerLastName;
                    verifiedorder.Address = pdt.addressstreet;
                    verifiedorder.City = pdt.addresscity;
                    verifiedorder.Country = pdt.addresscountry;
                    verifiedorder.State = pdt.addressstate;
                    verifiedorder.PostalCode = pdt.addresszip;
                    verifiedorder.Email = pdt.PayerEmail;
                    verifiedorder.TransactionID = pdt.TransactionId;
                    verifiedorder.PaymentStatus = pdt.PaymentStatus;
                    verifiedorder.PaymentGross = Convert.ToDecimal(pdt.PaymentGross);
                    verifiedorder.PaymentFee = Convert.ToDecimal(pdt.PaymentFee);
                    verifiedorder.AddressName = pdt.addressname;
                    verifiedorder.CountryCode = pdt.addresscountrycode;
                    db.SaveChanges();

                    var useremail = Membership.GetUser(verifiedorder.Username).Email;
                    var mailclient = new SmtpClient
                    {
                        Host = "relay-hosting.secureserver.net",
                        Port = 25
                    };

                    var mailclient1 = new SmtpClient
                    {
                        Host = "relay-hosting.secureserver.net",
                        Port = 25
                    };

                    var mailclient2 = new SmtpClient
                    {
                        Host = "relay-hosting.secureserver.net",
                        Port = 25
                    };

                    var smtpwrap = new SmtpClientWrapper(mailclient);
                    var smtpwrap1 = new SmtpClientWrapper(mailclient1);
                    var smtpwrap2 = new SmtpClientWrapper(mailclient2);
                    try
                    {
                        UserMailer.OrderInfo(orderid, useremail).Send(smtpwrap);
                        UserMailer.OrderInfo(orderid, pdt.PayerEmail).Send(smtpwrap1);
                        UserMailer.OrderInfo(orderid, "info@petsmartpm.com").Send(smtpwrap2);
                    }
                    catch (Exception)
                    {
                        
                    }
                }
                return RedirectToAction("Index", "ChargeSuccess", new { orderid });
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }
        }
    }
}

