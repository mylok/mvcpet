﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;
using FlcalPriceRD.ViewModels;
using Mvc.Mailer;
using MvcFocalPrice.Mailers;

namespace MvcFocalPrice.Controllers
{
    // [Authorize(Users="mylok")]
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            var db = new FocalpriceEntities();
            var categorycoll = db.Categories.Where(c => c.ParentID == 1 && c.IsActive == true).ToList();
            var recommendcoll = new List<Product>();
            //for (int i = 0; i < 5; i++)
            //{
            //    var recommendproduct = db.Products.Where(p=>p.IsAvailable == null || p.IsAvailable == true).OrderBy(p => Guid.NewGuid()).First();
            //    recommendcoll.Add(recommendproduct);
            //}
            var recomcoll = new List<int>();
            while (recomcoll.Count < 5)
            {
                var recommendproduct = db.Products.Where(p=>p.IsAvailable == null || p.IsAvailable == true).OrderBy(p => Guid.NewGuid()).First();
                if (!recomcoll.Contains(recommendproduct.ID))
                {
                    recomcoll.Add(recommendproduct.ID);
                    recommendcoll.Add(recommendproduct);
                }
            }
            ViewBag.recommendcoll = recommendcoll; //db.GetRecommendItems().ToList();
            return View(categorycoll);
        }
    }
}
