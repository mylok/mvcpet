﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using FlcalPriceRD;
using FlcalPriceRD.ViewModels;

namespace MvcFocalPrice.Controllers
{
    [Authorize]
    public class PaypalController : Controller
    {
        //
        // GET: /Paypal/

        public ActionResult Index()
        {
            //Process the order
            var cart = ShoppingCart.GetCart(this.HttpContext);
            var countrycode = Session["countrycode"].ToString(); 

            var shippingfee = cart.GetShippingFee(countrycode);
            var carttotal = cart.GetTotal();
            var grandtotal = carttotal + shippingfee;
            //ViewBag.shippingfee = (double)shippingfee;
            
            var db = new FocalpriceEntities();
            var order = new Order();
            order.Username = User.Identity.Name; 
            order.Orderdate = DateTime.Now;
            order.Shippingfee = shippingfee;
            order.Total = grandtotal;
            order.SelectCountry = countrycode;
            db.Orders.AddObject(order);
            db.SaveChanges();
            cart.CreateOrder(order, db);
            var coll = (from item in order.OrderDetails
                        select new PaypayViewModel {Shippingfee = (double)shippingfee, OrderID = item.OrderID, Productname = item.Product.Name, UnitPrice = (double)item.UnitPrice, Quantity = item.Quantity, Sku = item.Product.Sku }).ToList();
            return View(coll);
        }
    }
}
