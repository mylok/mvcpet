﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;

namespace MvcFocalPrice.Controllers
{
    public class ShoppingController : Controller
    {
        FocalpriceEntities db = new FocalpriceEntities();
        //
        // GET: /Shopping/

        public ActionResult Index()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);
            var db = new FocalpriceEntities();
            var coll = (from c in db.Countries
                       select new SelectListItem
                       {
                           Text = c.Name,
                           Value = c.Code
                       }).ToList();
            if (Session["countrycode"] != null)
            {
                var countrycode = Session["countrycode"].ToString();
                ViewBag.selectcountrycode = countrycode;
                ViewBag.shippingfee = cart.GetShippingFee(countrycode).ToString("C");
                ViewBag.ground = (cart.GetTotal() + cart.GetShippingFee(countrycode)).ToString("C");
                // initial select country 
                var item = coll.Single(t => t.Value.Trim() == countrycode);
                item.Selected = true;
            }
            else
            {
                coll.Insert(0, new SelectListItem { Text="-- Please select your country --", Value = "-1", Selected = true});
            }

            ViewBag.countrycoll = coll.OrderBy(t=>t.Text);

            // Set up ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal(),
                ItemTotal = cart.GetCount()
            };
            return View(viewModel);
        }

        public ActionResult AddToCart(int id, int qty)
        {
            // Retrieve the product from database
            var addedproduct = db.Products.Single(p => p.ID == id);
           
            // Add it to the shopping cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            cart.AddToCart(addedproduct, qty);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult RemoveFromCart(int productid, string cartid, string countrycode)
        {
            // Remove the item from the cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Get the name of the product to display confirmation
            var cartitem = db.ShoppingCarts
                .Single(item => item.ProductID == productid && item.CartID == cartid);
            var productname = cartitem.Product.Name;

            // Remove from cart
            //int itemCount = cart.RemoveFromCart(cartitem);
            db.ShoppingCarts.DeleteObject(cartitem);
            db.SaveChanges();

            var carttotal = cart.GetTotal();

            var shippingfee = cart.GetShippingFee(countrycode);

            var grandtotal = carttotal + shippingfee;

            if (shippingfee == -1m)
            {
                grandtotal = -1m;
            }


            // Display the confirmation message
            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(productname) +
                    " has been removed from your shopping cart.",
                CartTotal = carttotal,
                CartCount = cart.GetCount(),
                ItemCount = 0,
                DeleteId = cartitem.ProductID,
                GrandTotal = grandtotal,
                ShippingFee = shippingfee
            };

            return Json(results);
        }

        [HttpPost]
        public ActionResult CalculateShipping(string countrycode)
        {
            Session["countrycode"] = countrycode;
            var cart = ShoppingCart.GetCart(this.HttpContext);
            var shippingfee = cart.GetShippingFee(countrycode);

            var carttotal = cart.GetTotal();
            var grandtotal = carttotal + shippingfee;

            if (shippingfee == -1m)
            {
                grandtotal = -1m;
            }

            var results = new ShoppingCartCountryChangeViewModel
            {
                ShippingFee = shippingfee,
                GrandTotal = grandtotal
            };

            return Json(results);
        }

        [HttpPost]
        public ActionResult CartQtyChange(int productid, string cartid, int qty, string countrycode)
        {

            var db = new FocalpriceEntities();

            var cartitem = db.ShoppingCarts
                .Single(item => item.ProductID == productid && item.CartID == cartid);
            var productname = cartitem.Product.Name;

            cartitem.Quantity = qty;
            db.SaveChanges();

            var subtotal = (int?)qty * cartitem.Product.Price;

            var cart = ShoppingCart.GetCart(this.HttpContext);

            var carttotal = cart.GetTotal();

            var shippingfee = cart.GetShippingFee(countrycode);

            var grandtotal = carttotal + shippingfee;

            if (shippingfee == -1m)
            {
                grandtotal = -1m;
            }

            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(productname) +
                    " has been removed from your shopping cart.",
                CartTotal = carttotal,
                CartCount = cart.GetCount(),
                SubCartTotal = (Decimal)subtotal,
                ItemCount = 0,
                DeleteId = cartitem.ProductID,
                GrandTotal = grandtotal,
                ShippingFee = shippingfee
            };

            return Json(results);
        }

        [Authorize]
        public void ProcessPaypal(string grandtotal, string shippingfee, string selectcountry)
        {
            //return RedirectToAction("Index", "Paypal", new { grandtotal = Convert.ToDecimal(grandtotal), shippingfee = Convert.ToDecimal(shippingfee), selectcountry = selectcountry });
            RedirectToAction("Index", "Paypal");
        }

        // GET: /ShoppingCart/CartSummary

        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);  

            ViewBag.CartCount = cart.GetCount();

            return PartialView("CartSummary");
        }

    }
}
