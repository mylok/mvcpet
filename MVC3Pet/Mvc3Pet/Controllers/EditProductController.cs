﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;

namespace MvcFocalPrice.Controllers
{ 
    [Authorize(Users = "mylok")]
    public class EditProductController : Controller
    {
        private FocalpriceEntities db = new FocalpriceEntities();

        //
        // GET: /EditProduct/

        public ViewResult Index()
        {
            var products = db.Products;
            return View(products.ToList());
        }

        public ViewResult Index2()
        {
            var products = db.Products.Take(10);
            return View(products.ToList());
        }


        //
        // GET: /EditProduct/Details/5

        public ViewResult Details(int id)
        {
            Product product = db.Products.Single(p => p.ID == id);
            return View(product);
        }

        //
        // GET: /EditProduct/Create

        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories.Where(c=>c.ParentID != null), "ID", "Name"); 
            return View();
        } 

        //
        // POST: /EditProduct/Create

        [HttpPost]
        public ActionResult Create(Product product, string txtimg1, string txtimg2, string txtimg3, string txtimg4, string txtimg5, string txtimg6, string txtimg7, string txtimg8, string txtimg9)
        {
            if (ModelState.IsValid)
            {
                db.Products.AddObject(product);
                var newmatch = new productcategorymatch
                {
                    categoryid = product.CategoryID,
                    productid = product.ID
                };
                db.productcategorymatches.AddObject(newmatch); 
                db.SaveChanges();

                InsertPic(product, product.ImgName);
                InsertPic(product, txtimg2);
                InsertPic(product, txtimg3); 
                InsertPic(product, txtimg4);
                InsertPic(product, txtimg5);
                InsertPic(product, txtimg6);
                InsertPic(product, txtimg7);
                InsertPic(product, txtimg8);
                InsertPic(product, txtimg9);
                return RedirectToAction("Index");  
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", product.CategoryID);
            return View(product);
        }

        private void InsertPic(Product product, string txtimg)
        {
            if (!string.IsNullOrWhiteSpace(txtimg))
            {
                var pic = new ProductImage
                {
                    ProductID = product.ID,
                    IsFeature = true,
                    Name = txtimg
                };
                db.ProductImages.AddObject(pic);
                db.SaveChanges();
            }
        }
        
        //
        // GET: /EditProduct/Edit/5
 
        public ActionResult Edit(int id)
        {
            Product product = db.Products.Single(p => p.ID == id);
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", db.Categories.Single(t=>t.ID == product.CategoryID));
            ViewBag.Coll = db.Categories;
            ViewBag.CategoryName = db.Categories.Single(c => c.ID == product.CategoryID).Name;
            return View(product);
            //var model = new ProductEditModel
            //{
            //    SelectProductCategoryID = product.CategoryID,
            //    SelectProduct = product,
            //    CategoryColl = db.Categories
            //};
        }

        //
        // POST: /EditProduct/Edit/5

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Attach(product);
                db.ObjectStateManager.ChangeObjectState(product, EntityState.Modified); 
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            // ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", product.CategoryID);
            return View(product);
        }

        //
        // GET: /EditProduct/Delete/5
 
        public ActionResult Delete(int id)
        {
            Product product = db.Products.Single(p => p.ID == id);
            return View(product);
        }

        //
        // POST: /EditProduct/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Product product = db.Products.Single(p => p.ID == id);
            db.Products.DeleteObject(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}