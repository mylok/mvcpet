﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;
using MvcFocalPrice.Mailers;
using Mvc.Mailer;
using System.Net.Mail;

namespace MvcFocalPrice.Controllers
{ 
    [Authorize(Users = "mylok")]
    public class EditOrderController : Controller
    {

        private IUserMailer _userMailer = new UserMailer();
        public IUserMailer UserMailer
        {
            get { return _userMailer; }
            set { _userMailer = value; }
        }

        private FocalpriceEntities db = new FocalpriceEntities();

        public bool SendTrackingEmail(string orderid, string trackingnumber, string carrier)
        {
            var db = new FocalpriceEntities();
            var order = db.Orders.Single(p => p.OrderID == Int32.Parse(orderid));
            order.Carrier = carrier;
            order.TrackingNumber = trackingnumber;
            db.SaveChanges();
            try
            {
                var mailclient = new SmtpClient
                {
                    Host = "relay-hosting.secureserver.net",
                    Port = 25
                };

                var smtpwrap = new SmtpClientWrapper(mailclient);
                UserMailer.TrackingInfo(Int32.Parse(orderid), trackingnumber).Send(smtpwrap);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //
        // GET: /EditOrder/

        public ViewResult Index()
        {

            return View(db.Orders.OrderByDescending(t=>t.OrderID).ToList()); 
        }

        //
        // GET: /EditOrder/Details/5

        public ViewResult Details(int id)
        {
            Order order = db.Orders.Single(o => o.OrderID == id);
            return View(order);
        }

        //
        // GET: /EditOrder/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /EditOrder/Create

        [HttpPost]
        public ActionResult Create(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.AddObject(order);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(order);
        }
        
        //
        // GET: /EditOrder/Edit/5
 
        public ActionResult Edit(int id)
        {
            var coll = new List<SelectListItem>();
            coll.Add(new SelectListItem { Text = "USPS" });
            coll.Add(new SelectListItem { Text = "Royal Mail" });
            coll.Add(new SelectListItem { Text = "Posten" });
            coll.Add(new SelectListItem { Text = "Swisspost" });
            var order = db.Orders.Single(o => o.OrderID == id);
            if (order.Carrier != null)
            {
                var carrier = order.Carrier;
                var item = coll.Single(t => t.Text.Trim() == carrier);
                item.Selected = true;
            }
            ViewBag.carrier = coll;
            return View(order);
        }

        //
        // POST: /EditOrder/Edit/5

        [HttpPost]
        public ActionResult Edit(Order order, string carrier)
        {
            order.Carrier = carrier;
            if (ModelState.IsValid)
            {
                db.Orders.Attach(order);
                db.ObjectStateManager.ChangeObjectState(order, EntityState.Modified);
                db.SaveChanges();
                // return RedirectToAction("Index");
                var coll = new List<SelectListItem>();
                coll.Add(new SelectListItem { Text = "USPS" });
                coll.Add(new SelectListItem { Text = "Royal Mail" });
                coll.Add(new SelectListItem { Text = "Posten" });
                coll.Add(new SelectListItem { Text = "Swisspost" });
                if (order.Carrier != null)
                {
                    var ca = order.Carrier;
                    var item = coll.Single(t => t.Text.Trim() == ca);
                    item.Selected = true;
                }
                ViewBag.carrier = coll;
                return View();
            }
            return View(order);
        }

        //
        // GET: /EditOrder/Delete/5
 
        public ActionResult Delete(int id)
        {
            Order order = db.Orders.Single(o => o.OrderID == id);
            return View(order);
        }

        //
        // POST: /EditOrder/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Order order = db.Orders.Single(o => o.OrderID == id);
            db.Orders.DeleteObject(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}