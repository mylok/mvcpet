﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.IO;
using System.Net;
using FlcalPriceRD;

namespace MvcFocalPrice.Controllers
{
    public class ChargeSuccessController : Controller
    {
        //
        // GET: /ChargeSuccess/

        public ActionResult Index(int orderid)
        {
            var db = new FocalpriceEntities();
            var orderdetailcoll = db.OrderDetails.Where(p => p.OrderID == orderid).ToList();
            ViewBag.order = db.Orders.Single(p => p.OrderID == orderid);
            return View(orderdetailcoll);
        }

    }
}
