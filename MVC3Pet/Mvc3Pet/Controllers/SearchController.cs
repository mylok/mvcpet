﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlcalPriceRD;
using FlcalPriceRD.ViewModels;

namespace MvcFocalPrice.Controllers
{
    public class SearchController : Controller
    {
        // GET: /Home/
        public ViewResult Index(int pageno = 1, string keyword = "", string orderby = "lowestprice")
        {
            var db = new FocalpriceEntities();
            ViewBag.sort = orderby;
            ViewBag.pageno = pageno; 
            ViewBag.sort = orderby;
            ViewBag.categoryid = 1;
            ViewBag.catedescript = db.Categories.Single(c => c.ID == 1);
            ViewBag.categoryproeuctcountlist = null;
            var categorycoll = db.Categories.Where(c => c.ParentID == 1 && c.IsActive == true).ToList();
            var pcoll = db.Products.AsQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                pcoll = pcoll.Where(p => p.ItemDescription.Contains(keyword) || p.Name.Contains(keyword));
            }
            ViewBag.producttotal = pcoll.Count();
            ViewBag.totalpage = Math.Ceiling((double)pcoll.Count() / 50);
            var productcoll = new List<ProductListViewModel>();
            if (orderby == "lowestprice")
            {
                productcoll = (from pc in pcoll.Where(p => p.IsAvailable == true || p.IsAvailable == null).OrderBy(p => p.Price).Skip((pageno - 1) * 30).Take(30)
                               let picimg = pc.ProductImages.FirstOrDefault(x => x.IsFeature == true)
                               select new ProductListViewModel { ID = pc.ID, Name = pc.Name, Price = pc.Price, picname = picimg == null ? string.Empty : picimg.Name }).ToList();
            }
            else
            {
                productcoll = (from pc in pcoll.Where(p => p.IsAvailable == true || p.IsAvailable == null).OrderByDescending(p => p.Price).Skip((pageno - 1) * 30).Take(30)
                                   let picimg = pc.ProductImages.FirstOrDefault(x => x.IsFeature == true)
                                   select new ProductListViewModel { ID = pc.ID, Name = pc.Name, Price = pc.Price, picname = picimg == null ? string.Empty : picimg.Name }).ToList();
            }
            var homeview = new HomeViewModel
            {
                Productcoll = productcoll,
                Categorycoll = categorycoll
            };
            return View(homeview); 
        }
    }
}
