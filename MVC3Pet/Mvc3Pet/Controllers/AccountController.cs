﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using FlcalPriceRD;
using Mvc.Mailer;
using MvcFocalPrice.Mailers;
using FlcalPriceRD.ViewModels;
using MvcMemberShip.Models;

namespace MvcMemberShip.Controllers
{
    public class AccountController : Controller
    {
        private IUserMailer _userMailer = new UserMailer();
        public IUserMailer UserMailer
        {
            get { return _userMailer; }
            set { _userMailer = value; }
        }

        private void MigrateShoppingCart(string UserName)
        {
            // Associate shopping cart items with logged-in user
            var cart = ShoppingCart.GetCart(this.HttpContext);

            cart.MigrateCart(UserName);
            Session[ShoppingCart.CartSessionKey] = UserName;
        }

        //
        // GET: /Account/LogOn

        public ActionResult LogOn()
        {
            var db = new FocalpriceEntities();
            ViewBag.categorycoll = db.Categories.Where(c => c.ParentID == 1 && c.IsActive == true).ToList();
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    MigrateShoppingCart(model.UserName); 

                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            }

            // If we got this far, something failed, redisplay form
            var db = new FocalpriceEntities();
            ViewBag.categorycoll = db.Categories.Where(c => c.ParentID == 1 && c.IsActive == true).ToList();
            return View();
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    //// Add Shopping CartID To New User
                    //var db = new FocalpriceEntities();
                    //var userid = db.aspnet_Membership.OrderByDescending(u => u.CreateDate).First().UserId;
                    //var newuserinfo = UserInfo.CreateUserInfo(userid, Guid.NewGuid());
                    //db.UserInfoes.AddObject(newuserinfo);
                    //db.SaveChanges();
                    MigrateShoppingCart(model.UserName); 
                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", ErrorCodeToString(createStatus));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        //
        // GET: /Acount/ForgotPassword
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // Post: /Account/ForgotPassword
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                 MembershipUserCollection users = Membership.FindUsersByEmail(model.Email); 
                if (users.Count > 0)
                {
                    foreach (var u in users)
                    {
                        try
                        {
                            var password = ((MembershipUser)u).GetPassword();
                            UserMailer.PasswordRetrieve(model.Email, password).Send();
                        }
                        catch (Exception ex)
                        {
                            ViewBag.error = ex.ToString();
                        }
                    }
                    
                    return RedirectToAction("LogOn");
                }
            }

            // If we got this for, something failed,
            // redisplay form
            return View(model);
        }

        [Authorize]
        public ActionResult Index()
        {
            var db = new FocalpriceEntities();
            var categorycoll = db.Categories.Where(c => c.ParentID == 1 && c.IsActive == true).ToList();
            var username = User.Identity.Name;
            ViewBag.ordercoll = (from o in db.Orders.Where(u => u.Username == username).ToList()
                                 let detailcoll = o.OrderDetails.ToList()
                                 select new OrderViewModel { orderinfo = o, orderdetailcoll = detailcoll }).ToList();
            return View(categorycoll); 
        }

        public ActionResult GetOrderDetails(int orderid)
        {
            var db = new FocalpriceEntities();
            var detailcoll = db.OrderDetails.Where(o => o.OrderID == orderid).ToList();
            return View(detailcoll);
        }
        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
