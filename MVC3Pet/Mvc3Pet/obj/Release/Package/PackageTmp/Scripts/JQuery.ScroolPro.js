/*
* 2011/9/7
* author MixSun
*/
;(function($) {
    $.extend({
        ScroolPro: function(options) {
            var op = $.extend({
                preBtn: ".prev",
                nextBtn: ".next",
                wrap: ".v_show",   //all Div
                listwrap: ".v_content_list",//滚动总体Div	
                content: ".v_content",		//外围Div
				disableClass:"disableClass", //不可点击class
				listcon:".v_content_listcon",
                page: 1,
                i: 4
            },options);
            var page = op.page;
            var i = op.i;
            var $parent = $(op.wrap); //根据当前点击元素获取到父元素
            var $v_show = $parent.find(op.listwrap); //寻找到"视频内容展示区域"
            var $v_content = $parent.find(op.content); //寻找到"视频内容展示区域"外围的DIV元素
            var v_width = $v_content.width();
            var len = $v_show.find(op.listcon).length;
            var page_count = Math.ceil(len / i);
            $(op.nextBtn).live('click', function(){
				if (!$v_show.is(":animated")) { //判断"视频内容展示区域"是否正在处于动画
                    if (page == page_count) { 
                       // alert(page_count);
						return false;
                    } else {
                        $v_show.animate({
                            left: '-=' + v_width
                        },"fast"); 
						$(op.preBtn).removeClass(op.disableClass);
                        page++;
						if (page == page_count){
							$(this).addClass(op.disableClass);	
						}
                    }
                }	
			});
            $(op.preBtn).live('click', function(){
				if (!$v_show.is(":animated")) { //判断"视频内容展示区域"是否正在处于动画
                    if (page == 1) { 
                        $(this).addClass(op.disableClass);
						return false;
                    } else {
                        $v_show.animate({
                            left: '+=' + v_width
                        },"fast");
                        page--;
						$(op.nextBtn).removeClass(op.disableClass);
						if (page == 1){
							$(this).addClass(op.disableClass);	
						}
                    }

                }	
			});
        }
    });
})(jQuery);