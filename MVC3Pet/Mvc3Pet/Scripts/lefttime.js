function leftTime(d,h,m,s,o,endTime,callfn){
	var _this=this;
	_this.o=o;
	_this.endTime=endTime;
	_this.callfn=callfn;
	_this.Interval=window.setInterval(function(){
		_this.endTime-=1000;
		_this.startTime(_this.endTime);
	},1000);
	_this._d=d;
	_this._h=h;
	_this._m=m;
	_this._s=s;
}
leftTime.prototype={
	startTime:function(t){
		var time="";
		var a=t;
		if (a<1){
			window.clearInterval(this.Interval);
			this.callfn();
			return;
		}
		var day=parseInt(a/86400000);
		a=a-day*86400000;
		var hour=parseInt(a/3600000);
		a=a-hour*3600000;
		var m=parseInt(a/60000);
		a=a-m*60000;
		var s=parseInt(a/1000);
		var hours = this._d+hour;
		var mh = this._h+m
		var ms = this._m+s;
		if(day == 0){
			this.o.innerHTML="<em>"+hours+"&nbsp;</em>H<em>&nbsp;"+mh+"&nbsp;</em>M<em>&nbsp;"+ms+"&nbsp;</em>S";		
			if(day == 0 && hours == 0){
				this.o.innerHTML="<em>"+mh+"&nbsp;</em>M<em>&nbsp;"+ms+"&nbsp;</em>S";
				if(day == 0 && hours == 0 && mh == 0){
					this.o.innerHTML="<em>&nbsp;"+ms+"&nbsp;</em>S";
					if(day == 0 && hours == 0 && mh == 0 && ms == 1){
						window.location.reload();
					}
				}			
			}
		}else{
			this.o.innerHTML="<em>&nbsp;"+day+"&nbsp;</em>Day"+"&nbsp;<em>&nbsp;"+hours+"&nbsp;</em>H<em>&nbsp;"+mh+"&nbsp;</em>M<em>&nbsp;"+ms+"&nbsp;</em>S";
		}
	}
};
