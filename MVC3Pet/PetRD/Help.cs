﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlcalPriceRD;

namespace FlcalPriceRD
{
    public class Help
    {
        /// <summary>
        /// get bottom subcategory(no child category)
        /// </summary>
        /// <param name="categoryid">parentcategory</param>
        /// <param name="tcoll">collection</param>
        /// <param name="parentidcoll">table parentid collection</param>
        public void GetCategoryList(int categoryid, List<int> tcoll)
        {
            var db = new FocalpriceEntities();
            var parentidcoll = db.Categories.Select(c => c.ParentID).Distinct().ToList();
            if (parentidcoll.Contains(categoryid))
            {
                var coll = db.Categories.Where(c => c.ParentID == categoryid).Select(c => c.ID);
                foreach (var item in coll)
                {
                    GetCategoryList(item, tcoll);
                }
            }
            else
            {
                tcoll.Add(categoryid);
            }
        }

        public void GetParentID(int? id, List<int?> coll)
        {
            if (id == null)
            {
                return;
            }
            coll.Add(id);
            var db = new FocalpriceEntities();
            var pid = db.Categories.Single(c => c.ID == id).ParentID;
            if (pid != null && pid != 1)
            {
                coll.Add(pid);
                GetParentID(pid, coll);
            }
        }
    }
}
