﻿namespace FlcalPriceRD
{
    public class ShoppingCartCountryChangeViewModel
    {
        public decimal GrandTotal { get; set; }
        public decimal ShippingFee { get; set; }
    }
}
