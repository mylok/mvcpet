﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FlcalPriceRD
{
    [MetadataType(typeof(Product_Html))]
    public partial class Product
    {
    }

    public class Product_Html
	{
        [AllowHtml]
        public string ItemDescription { get; set; }
	}
}
