﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlcalPriceRD;

namespace FlcalPriceRD.ViewModels
{
    public class ProductListViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string picname { get; set; }
    }

    public class HomeViewModel
    {
        public List<ProductListViewModel> Productcoll { get; set; }
        public List<Category> Categorycoll { get; set; }
    }
}
