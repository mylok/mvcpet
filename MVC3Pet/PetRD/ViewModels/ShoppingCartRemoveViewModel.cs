﻿namespace FlcalPriceRD
{
    public class ShoppingCartRemoveViewModel
    {
        public string Message { get; set; }
        public decimal CartTotal { get; set; }
        public int CartCount { get; set; }
        public int ItemCount { get; set; }
        public decimal SubCartTotal { get; set; }
        public int DeleteId { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal ShippingFee { get; set; }
    }
}