﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlcalPriceRD.ViewModels
{
    public class PaypayViewModel
    {
        public int OrderID { get; set; }
        public string Productname { get; set; }
        public double UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string Sku { get; set; }
        public double Shippingfee { get; set; }
    }
}
