﻿using System.Collections.Generic;
using FlcalPriceRD;

namespace FlcalPriceRD
{
    public class ShoppingCartViewModel
    {
        public List<ShoppingCart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
        public int ItemTotal { get; set; }
    }
}