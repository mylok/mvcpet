//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlcalPriceRD
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        public Order()
        {
            this.OrderDetails = new HashSet<OrderDetail>();
        }
    
        public int OrderID { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<System.DateTime> Orderdate { get; set; }
        public Nullable<bool> IsCompleted { get; set; }
        public Nullable<bool> IsVerify { get; set; }
        public string AddressName { get; set; }
        public string CountryCode { get; set; }
        public Nullable<decimal> PaymentGross { get; set; }
        public Nullable<decimal> PaymentFee { get; set; }
        public string TransactionID { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<decimal> Shippingfee { get; set; }
        public string SelectCountry { get; set; }
        public Nullable<System.Guid> CustomerID { get; set; }
        public string Carrier { get; set; }
        public string TrackingNumber { get; set; }
    
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
